# [Download this, do not just clone the git](https://gitlab.com/n4gi0s/vu-mapvote/-/jobs/artifacts/master/download?job=build)

### Contributors:
- @ensio
- @YBinnenweg

## Installation:

1. Extract contect of zip to your mods folder. This should look something like Admin/Mods/vu-mapvote
2. Add vu-mapvote to your Admin/ModList.txt
3. Setup the maps you want in MapList.txt (without this it will **not** work! See example below)
4. Voting starts at the endscreen

## Optional configuration:

Add these to your Admin/Startup.txt

	mapvote.randomize <true/false> (default true)
	mapvote.limit <number of selectable random maps> (default 15)
	mapvote.excludecurrentmap <true/false> (default true)

## Manually starting a vote:

	mapvote.start
	mapvote.end

## In Game:

![](https://i.ibb.co/VDw63KV/1.png)

![](https://i.ibb.co/wwLzbdf/2.png)

![](https://i.ibb.co/c3yF0gM/5.png)

## MapList.txt example

```txt
MP_001 ConquestLarge0 1
MP_003 ConquestLarge0 1
MP_007 ConquestLarge0 1
MP_011 ConquestLarge0 1
MP_012 ConquestLarge0 1
MP_013 ConquestLarge0 1
MP_017 ConquestLarge0 1
MP_018 ConquestLarge0 1
MP_Subway ConquestLarge0 1
XP1_001 ConquestAssaultLarge0 1
XP1_002 ConquestAssaultLarge0 1
XP1_003 ConquestAssaultLarge0 1
XP1_004 ConquestAssaultLarge0 1
```
